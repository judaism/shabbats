.. index::
   pair: Shabbat ; Vaera
   pair: Ecologie ; https://poursurmelin.files.wordpress.com/2024/01/judaisme-et-ecologie.pdf

.. _vaera_2024_03_12:

================================================================================================================================================================================
2024-01-12 Shabbat Vaera 💚Chabat Chalom! Un grand chabat se prépare: Ecologie (10h), Rendez-vous des ados (11h30), Leçon rabbinique (13h15), Book-club (14h)+ BM adulte💚
================================================================================================================================================================================

- https://rabbinchinsky.fr/2024/01/12/chabat-vaera/
- https://poursurmelin.files.wordpress.com/2024/01/judaisme-et-ecologie.pdf

Introduction
================

Ce Chabbat, nous célébrons une BM adulte.

C’est un grand moment, la réaffirmation que le judaïsme appartient à toutes
et toutes.

En conséquence, l’office de ce soir est à Surmelin (18h45), tout comme celui
de demain matin (10h30).

Demain
==========

- 10h  les 20 minutes du Rabbin, sur le thème  » L’écologie, le judaïsme et nous  »
- 11h15 le Rendez-vous des ados
- 12h30- Kidouch
- 13h15- Leçon rabbinique
- 14h- Book-Club.


Les lectures de ce chabbat sont les suivantes: https://www.hebcal.com/sedrot/vaera-20240113

École des Apprentis-sages
=============================

Jeudi prochain, j’aurai le plaisir d’intervenir à l’École des Apprentis-sages,
sur le thème  "Talmud, Art du désaccord pacifique et CNV", info à suivre, réservez votre soirée!

vidéo
============

.. youtube:: FawJdzr9EX8

La section de la Bible de la semaine en trois points, une phrase et une
question... Répondez dans les commentaires ;-).

Ceci est juste une petite partie de l'Enseignement, le reste, ce sont des
commentaires, à approfondir...

#paracha #rabbinchinsky #judaïsme #féminisme #sagesses


Ressources pour les études de ce chabbat
============================================

- `Judaisme et écologie <https://poursurmelin.files.wordpress.com/2024/01/judaisme-et-ecologie.pdf>`_
