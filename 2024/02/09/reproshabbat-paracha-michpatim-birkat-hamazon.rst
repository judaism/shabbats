

===============================================================================
2024-02-09 💚 **Reproshabbat – paracha michpatim – birkat hamazon** 💚
===============================================================================

#reproshabbat #judaismeinclusif #prochoice; @ncjwinc @jewishwomenarchives.
@jewishwomensarchive @ncjwinc #prochoice #liberté #judaismeinclusif #judaisme #FlorianeChinsky

Ce chabbat est le reproshabbat, chabbat de la liberté reproductive,

J’en parle également sur instagram et dans le commentaire de la paracha de la
semaine sur youtube, ici:


Vidéo
===========

.. youtube:: Ej6JaL1a_bk


Reproshabbat - chabbat de la liberté du choix d'engendrer et paracha michpatim...

Rearmons nos libertés ! #reproshabbat, chabbat de la défense de la liberté
reproductive.

@jewishwomensarchive @ncjwinc
#prochoice #liberté #judaismeinclusif #judaisme

Le Birkat hamazon
=======================

Le Birkat hamazon est un temps essentiel de la vie juive.

Vous voulez apprendre à le faire en 2 minutes?

Je vous ai enregistré la version de Markus Kohn, belle, traditionnelle et courte.
Vous la trouverez ici:

.. youtube:: W0mFFaW9FHQ


Prendre un moment après le repas pour se réjouir de notre chance de manger à
notre faim, et retrouver des forces pour agir autour de nous, tel est l'objectif
du birkat hamazon.

Voici la version en rimes écrite par "le grand Kohn" tel qu'il était surnommé
dans ma famille, qui a été directeur de l''Ecole Maïmonide à Paris et auteur
du dictionnaire Larousse hébreu-français.

**Mazon (nourriture), y rime avec Hazon (vision)**. Tout un programme!


Prochain chabat Kesher le 16 mars 2024 , infos ici: https://rabbinchinsky.fr/2024/02/08/%f0%9f%92%9aloffice-de-toutes-et-tous-prochain-rendez-vous-le-16-mars-toutes-les-infos/


Discours contre a peine de mort
====================================

Et petit rappel sur l’un des combats de Robert Badinter…

.. youtube:: x8w1kIAnmYo

Après l’arrivée de la gauche au pouvoir en 1981, le ministre de la justice,
Robert Badinter porte le projet de loi d’abolition de la peine de mort, mettant
ainsi en œuvre une promesse de campagne du président François Mitterrand.

Le 17 septembre 1981, dans un discours enflammé de plus de deux heures tenu
devant l’Assemblée nationale, il déclare : « J’ai l’honneur, au nom du Gouvernement
de la République, de demander à l’Assemblée nationale l’abolition de la peine
de mort en France… ».

Puis il ajoute : « Demain, grâce à vous, la justice française ne sera plus
une justice qui tue (…). Demain, vous voterez l’abolition de la peine de mort »,

Le projet de loi est adopté le lendemain par les députés, et le 30 septembre
par les sénateurs. La loi « portant abolition de la peine de mort » est
promulguée le 9 octobre 1981.

25 ans plus tard, le 19 février 2007, l’abolition est inscrite dans la
Constitution par le Parlement réuni en Congrès à Versailles.

Robert Badinter déclare alors : « La peine de mort est vouée à disparaître
de ce monde comme la torture, parce qu’elle est une honte pour l’humanité.

Jamais, nulle part, elle n’a fait reculer la criminalité sanglante. »


