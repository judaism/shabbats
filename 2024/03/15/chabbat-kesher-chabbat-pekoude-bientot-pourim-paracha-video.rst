.. index::
   pair: Shabbat ; Pekoude


.. _pekoude_2024_03_15:

=========================================================================================
2024-03-15 💚 **Chabbat KESHER! Chabbat Pékoudé + Bientôt Pourim! + paracha vidéo!**
=========================================================================================

- https://rabbinchinsky.fr/2024/03/15/chabbat-kesher/
- https://rabbinchinsky.fr/2019/03/13/tout-pour-preparer-pourim/

#surunpied #sup2024 #Pekudei #pékoudé #judaïsme #exode #paracha #bible #sagesses #FlorianeChinsky

Vidéo
========

.. youtube:: vlTZ9xGas4k


Comment tourner la page sans reléguer le livre ?

Comment garder l'énergie du service du temple tout en passant à la pratique
personnelle ?

Comment instaurer la torah orale sans perdre la force de la torah écrite?

En lisant les règles qu'on ne va pourtant pas appliquer.

Plus d'explications sur cette vidéo. Chabbat chalom!


Chabbat Kesher, c’est le chabbat participatif! De nombreux.e membres se
relaieront à la téva demain. Venez partager leur joie et leur Kidouch!
Ce soir office Pelleport 18h45, demain office surmelin 10h30.


SAVE THE DATE : Le prochain office KESHER est le 27 avril, pendant la semaine
de PessaH. L’enregistrement des lectures de la Torah sera en ligne des motsa »ch!

SAVE THE DATE: POURIM approche, on vous réserve un super programme.
RV SAMEDI 23 MARS 2024 fin d’après-midi et DIMANCHE 24 MARS 2024 matin.

Pour prendre une part plus active au programme: contactez-moi en commentaire
de cet article.
De nombreuses ressources ici : https://rabbinchinsky.fr/2019/03/13/tout-pour-preparer-pourim/

Pour lire à l’avance la haftara, c’est ici!

- https://poursurmelin.files.wordpress.com/2024/03/haftara-pekoude.pdf

et pour apprendre les taamim

- c’est là : https://poursurmelin.files.wordpress.com/2024/03/tout-pour-apprendre-les-taamim.pdf
- et là https://rabbinchinsky.fr/judaisme-pratique/lire-torah-haftara/


Chabbat Chalom à toutes et tous! – שבת שלום
