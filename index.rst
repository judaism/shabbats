.. index::
   pair: Shabbats ; Rav Floriane Chinsky
   ! Shabbats

.. un·e
.. ❤️💛💚

.. raw:: html

   <a rel="me" href="https://babka.social/@pvergain"></a>
   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://kolektiva.social/@noamsw"></a>


|FluxWeb| `RSS <https://judaism.gitlab.io/shabbats/rss.xml>`_

.. _shabbats:

===============================================================================
|floriane| **Shabbats du Rav Floriane Chinksy**
===============================================================================

- :ref:`chinsky:floriane_chinsky`
- https://rabbinchinsky.fr/
- https://rabbinchinsky.fr/about/rabbin-floriane-chinsky/
- https://fr.wikipedia.org/wiki/Floriane_Chinsky


.. toctree::
   :maxdepth: 6

   2024/2024
   2023/2023
   glossaire/glossaire
   ressources/ressources
