.. index::
   ! glossaire

.. _judaisme_glossaire:

=====================================================================================================
Glossaire
=====================================================================================================


.. glossary::

   Michna
       Source: "Des femmes et des dieux".
       Première strate de la rédaction, au IIe siècle, d'éléments de la
       Torah orale. La Michna comprend des opinions de rabbins sur les
       pratiques à suivre, l'exposé de déaccords entre eux, des histoires
       et des versets de la Torah écrite utilisés pour faire avancer les
       débats.

   Kavod
       https://www.france.tv/france-2/a-l-origine/5320821-horizon-le-kavod.html

       Le Kavod est un principe moral et spirituel du judaïsme, qui
       évoque le respect.

       Il est très important dans cette religion.

       Nous en parlons aujourd'hui avec le Rabbin Michaël Azoulay.

