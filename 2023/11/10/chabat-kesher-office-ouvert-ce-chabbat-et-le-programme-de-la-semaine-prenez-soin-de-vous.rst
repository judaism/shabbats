.. index::
   pair: Shabbats ; 2023-11-10
   pair: Paracha ; Haye Sarah (5)

.. _shabbat_2023_11_10:

=======================================================================================================
💚 **Chabat Kesher**, Paracha Haye Sarah (5)
=======================================================================================================

- https://rabbinchinsky.fr/2023/11/10/%f0%9f%92%9achabat-kesher-office-ouvert-ce-chabbat-et-le-programme-de-la-semaine-prenez-soin-de-vous/
- https://invidious.fdn.fr/watch?v=C_631s4eRgk (Paracha Haye Sarah 💚Partir ? Rester ? Le Dilemme de Rébecca - avec diapo et sources)
- :ref:`antisem:pins_hay`


Programme de ce chabbat
============================

Programme de ce chabbat

- RV ce soir à Pelleport à 18h45,
- demain:

  - accueil à partir de 10h à Surmelin,
  - office Kesher à 10h30,
  - Contes de Hanouka et book club à 13h,
  - Paracha à 13h30
  - et Chorale de Roch Hodech à 14h.

:download:`Feuille de sources pour ce chabbat: <doc/chabat-haye-sarah-2-haftara.docx>`


Cette semaine
=================

- rv avec les femmmes du mur le 14 novembre 2023 à 6h du matin: https://rabbinchinsky.fr/wow/,
- rv avec le leadership juif le 16 novembre 2023 à 19h: https://rabbinchinsky.fr/leadership-5784/
  inscriptions: https://framaforms.org/cours-leadership-juif-1697361226


**Paracha Haye Sarah** 💚 **Partir ? Rester ? Le Dilemme de Rébecca** - avec diapo et sources
=====================================================================================================

- https://invidious.fdn.fr/watch?v=C_631s4eRgk (💚Partir? Rester? Le Dilemme de Rébecca - avec diapo et sources)

Comment Rébecca décide-t-elle de quitter sa famille pour partir
à l'aventure ?

Quel exemple nous donne cette grande figure biblique ?

En quoi éclaire-t-elle l'idée qu'il faut ouvrir les possibles et sortir
de la vision binaire ?


**Paracha Haye Sarah : Partir ou rester, le dilemme de Rebecca**
====================================================================

- https://fr.wikipedia.org/wiki/Hayei_Sarah
- https://invidious.fdn.fr/watch?v=7rRa_mfLMbk
- https://www.hebcal.com/sedrot/chayei-sara-20231111


Sat, 11 November 2023 ; 27th of Cheshvan, 5784 ; Shabbat Mevarchim Chodesh Kislev ; Parashat Chayei Sara 5784 פָּרָשַׁת חַיֵּי שָֹרָה
----------------------------------------------------------------------------------------------------------------------------------

- https://www.hebcal.com/sedrot/chayei-sara-20231111

**Parashat Chayei Sara is the 5th weekly Torah portion** in the annual Jewish
cycle of Torah reading.

Torah Portion: Genesis 23:1-25:18

Chayei Sarah (“The Life of Sarah”) opens as Sarah dies and Abraham buys
the Cave of Machpelah to bury her. Abraham sends his servant to find a
wife for Isaac.
The servant meets Rebecca at a well, and Rebecca returns with the servant
to marry Isaac.
Abraham remarries, has more children, and dies at age 175.


https://reformjudaism.org/torah/portion/chayei-sarah
----------------------------------------------------------

- https://reformjudaism.org/torah/portion/chayei-sarah


https://www.sefaria.org/topics/parashat-chayei-sarah?sort=Relevance&tab=sources
----------------------------------------------------------------------------------

- https://www.sefaria.org/topics/parashat-chayei-sarah?sort=Relevance&tab=sources

https://fr.wikipedia.org/wiki/Hayei_Sarah
----------------------------------------------

- https://fr.wikipedia.org/wiki/Hayei_Sarah

Hayei Sarah, Haye Sarah, ou Hayye Sarah (hébreu : חיי שרה « la vie de Sarah »)
est la cinquième parasha (section hebdomadaire) du cycle annuel juif de
lecture de la Torah.

Elle est constituée de Genèse 23:1–25:18.

Les Juifs de la Diaspora la lisent le cinquième Shabbat après Sim'hat
Torah, généralement en novembre.

https://ajr.edu/divrei-torah/?_by_parsha=hayei-sarah
----------------------------------------------------------

- https://ajr.edu/divrei-torah/?_by_parsha=hayei-sarah


https://www.jtsa.edu/jts-torah-online/?parashah=hayyei-sarah
-----------------------------------------------------------------

- https://www.jtsa.edu/jts-torah-online/?parashah=hayyei-sarah


Message des Facilitant.es Kesher
=======================================

.. figure:: images/offices.png

Hello everyone ! Vous avez manifesté votre intérêt pour les offices Kesher !

Offices kesher, quésaco ? Les offices Kesher sont offices participatifs
où chacun devient Chaliah tzibour (dirigeant de l’office) à son tour !

Il s’agit de prendre un charge une partie entière de l’office OU un/
des psaume.s selon l’air traditionnel de Surmelin ou avec la possibilité
d’introduire un nouvel air !

Afin de nous préparer au mieux pour les offices kesher, nous vous
proposons de manifester quelle partie ou quel.s psaume.s vous souhaiteriez
diriger pour l’office et si vous voulez reprendre l’air traditionnel
ou un autre air.

Avis aux plus courageux d’entre nous, vous trouverez en pièce jointe le
découpage de l’office en différentes parties si cela vous tente.

Nous vous proposons par la suite d’organiser une réunion sur zoom ou en
présentiel pour une répétition dans la semaine afin que nous soyons
tous synchro le jour J

Le lancement des offices kesher aura lieu le Samedi 11 novembre 2023 à la
synagogue JEM Surmelin.

Les prochains Kesher : 09/12, 09/03, 27/04, 4/05, 25/05, 1/06, 22/06.

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.4013414978981023%2C48.867294339093895%2C2.4048820137977605%2C48.86872875238153&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/48.86801/2.40311">Afficher une carte plus grande</a></small>
