.. index::
   pair: Paracha Vayéchev; 2023-11-24

.. _paracha_vayechev_2023_11_24:

==============================================================================================================================================================
2023-11-24 **Paracha Vayéchev : Le mérite des femmes justes** Parashat Vayetzei is the 7th weekly Torah portion in the annual Jewish cycle of Torah reading
==============================================================================================================================================================

- https://rabbinchinsky.fr/2023/11/24/%f0%9f%92%9achabat-chalom-20-minutes-du-rabbin-demain-10h/
- https://www.youtube.com/watch?v=ORev_TWE6eg
- https://www.hebcal.com/sedrot/vayetzei-20231125


Les lectures de ce chabbat sont les suivantes: https://www.hebcal.com/sedrot/vayetzei-20231125

- `Vidéo Paracha de la semaine et Hanouka ici <https://www.youtube.com/watch?v=ORev_TWE6eg>`_,       
- `Vidéos Boker Tov spécial Hanouka ici <https://www.youtube.com/playlist?list=PLnHlXjFx9rOSI2tIzKbfGgaQdOLL1itdp>`_
