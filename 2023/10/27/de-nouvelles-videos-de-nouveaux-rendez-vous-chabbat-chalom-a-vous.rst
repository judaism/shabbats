
.. index::
   pair: Parashat ; Lech-Lecha (3)

.. _shabbat_2023_10_27:

=======================================================================================================================
2023-10-27 💚 **De nouvelles vidéos, de nouveaux rendez-vous, chabbat chalom à vous** 💚 Parashat Lech-Lecha 5784
=======================================================================================================================

- https://rabbinchinsky.fr/2023/10/27/%f0%9f%92%9ade-nouvelles-videos-de-nouveaux-rendez-vous-chabbat-chalom-a-vous%f0%9f%92%9a/
- https://www.hebcal.com/sedrot/noach-20231021?i=on

Introduction
===============

Cher toutes et tous, je vous retrouve non pas ce chabbat, mais les
2, 3 et 4 novembre 2023 autour de la Bat Mitsva de notre amie Marianne.

**Un grand mazel tov à elle et aux nombreuses personnes qui font cette
démarche à l’age adulte, renforçant la puissance active de notre communauté**.

Comme toujours, offices du soir 18h45 Pelleport, 10h30 Surmelin.
8h30 pour l’office du jeudi matin, apportez vos téfilines!



Lectures de ce chabbat ici: https://www.hebcal.com/sedrot/noach-20231021?i=on
=================================================================================

Lectures de ce chabbat ici: https://www.hebcal.com/sedrot/noach-20231021?i=on

Parashat Lech-Lecha 5784 / פָּרָשַׁת לֶךְ־לְךָ
-------------------------------------------

Parashat Lech-Lecha is the 3rd weekly Torah portion in the annual Jewish
cycle of Torah reading.

Torah Portion: Genesis 12:1-17:27
----------------------------------------

Lech Lecha (“Go Forth”) recounts Abraham’s (here known as Abram) first
encounter with God, his journey to Canaan, the birth of his son Ishmael,
the covenant between him, his descendants, and God, and God’s commandment
to circumcise the males of his household.


Prenez particulièrement soin de vous en ces temps difficiles
==============================================================

**Prenez particulièrement soin de vous en ces temps difficiles.
Pour vous, et pour vos enfants**.

Voici les recommandations du ministère de la santé israélien par exemple:
https://youtu.be/jr5xMikRNKU

Par ailleurs, ma chaine youtube propose des vidéos non reliées à la situation,
ce qui fait également du bien:

- Sur Abraham et le Midrach ici: youtu.be/jskCOYYa_io;
- sur Eve et la Bible ici; https://youtu.be/76WlujBzvSo ,
- sur l’héroïsme des femmes vue par les rabbins du Talmud ici https://youtu.be/B0RSZ2-rWK4
- et ici https://youtu.be/eSBT6xwUvfo à l’occasion du nouveau mois.


Je fais également des traductions de reportages de la télévision israélienne
comme ❤️ traduction:

- `La décision de détruire le poste de police de Sderot (qui a reçu beaucoup de vues) <https://youtu.be/_PyBzekB9Ww>`_
- et 🧡 `Traduction: Hibouki, la poupée de thérapie (qui en mérite au moins autant) <https://youtu.be/tTCCNhQJ3f4>`_

La semaine prochaine, je ferai entre autre une vidéo sur les influenceurs
et influenceuses arabes israéliens qui militent pour une image équilibrée
de ce qui se passe en Israël.

Abonnez-vous à ma chaîne youtube pour tout recevoir, likez pour agir en
faisant remonter l’algorithme, commentez pour que je sache ce que vous
pensez et que je vous réponde.

Si Youtube vous fatigue, ces vidéos sont également disponibles sur peertube,
cliquez sur les liens en fin d’article.

Joignez-vous à la conf en ligne du lundi 6 novembre 2023, inscrivez-vous
ici si ce n’est déjà fait: https://rabbinchinsky.fr/2023/10/24/conf-judaisme/

Pour demander un rendez-vous avec moi, c’est ici: https://framaforms.org/demande-de-rendez-vous-rabbin-chinsky-2023-1696317774

Nous restons ensembles, pour le meilleur. Chabbat chalom
=============================================================

**Nous restons ensembles, pour le meilleur. Chabbat chalom**.


Les vidéos sur peertube, sans pub
====================================

- ❤️🧡💛💚 `Prévenir pour choisir mettre en place des trigger warning <https://peertube.iriseden.eu/w/hXjawwtAKGZTcDBbhL73iU>`_
- 💚 `Hechvan Femmes et héroïsme <https://peertube.iriseden.eu/w/sVqtGtBV8Zqe1Zwxoj1R9u>`_
- 💚 `Debunkible #1 debunkons la Bible et le personnage de Eve <https://peertube.iriseden.eu/w/vfPz6dEpyyjQxfwDpf9n1o>`_
- 💚 `Celui qui avait toujours raison et voulait le prouver… Un conte talmudique <https://peertube.iriseden.eu/w/57rUrqAwnCBb8oerDQmsN5>`_
- 💚 `Étude Hechvan Femmes et héroïsme <https://peertube.iriseden.eu/w/sVqtGtBV8Zqe1Zwxoj1R9u>`_
- 🧡 `Ça se passe ici #1 cris – sécurité <https://peertube.iriseden.eu/w/cBSsApdWZpKcgi3XRzoT2h>`_
- 🧡 `Comment désamorcer la violence <https://peertube.iriseden.eu/w/eeDi6qsVBp3xhtqcnBFVBV>`_
- ❤️🧡 `Comment combattre tout en restant humain traduction-résumé d’un reportage <https://peertube.iriseden.eu/w/726FRrCqiL4QMVfmnJTJ99>`_
- ❤️ `Ce sont des moments difficiles, nous les traverserons ensemble traduction résumée d’un clip du ministère de la santé <https://peertube.iriseden.eu/w/6Kny181Hxv5geQh3KWVgGz>`_
- ❤️🧡  `Femmes et héroïsme, étude complète des sources <https://peertube.iriseden.eu/w/5xgYt39LA5quZj8aCdMYLh>`_

