.. index::
   ! Ressources
   ! Hebrew calendar

.. _ressources:

===============================================================================
**Ressources**
===============================================================================


|floriane| **Rav Floriane Chinsky**
======================================

- :ref:`chinsky:floriane_chinsky`


|sefaria| **Sefaria Project**
=================================

- https://www.sefaria.org/texts


.. _hebcal:

|hebcal| Hebcal
=====================

- https://www.hebcal.com/
- https://www.hebcal.com/home/developer-apis

Hebcal (pronounced HEEB-kal, as in **Hebrew calendar**) is a free Jewish
calendar and holiday web site.

Open source Judaism
======================

- https://en.wikipedia.org/wiki/Open_Source_Judaism

|paix| Guerrières de la Paix |hanna|
==========================================

- :ref:`guerrieres:paix`
